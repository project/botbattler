<?php

namespace Drupal\botbattler\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Administrative settings for BotBattler.
 */
class BotBattlerConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'botbattler_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['botbattler.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('botbattler.settings');

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('BotBattler configuration'),
    ];
    $form['settings']['submit_wait'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BotBattler minimum form submission wait time'),
      '#description' => $this->t('Minimum time required before pressing Submit is effective.'),
      '#default_value' => $config->get('submit_wait'),
      '#required' => TRUE,
      '#size' => 4,
      '#field_suffix' => $this->t('seconds'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $submit_wait = $form_state->getValue('submit_wait');
    if (!is_numeric($submit_wait) || ($submit_wait <= 0.0)) {
      $form_state->setErrorByName('submit_wait', $this->t("The submit wait time must be a number greater than zero."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('botbattler.settings');
    $config->set('submit_wait', $form_state->getValue('submit_wait'))->save();
    parent::submitForm($form, $form_state);
  }

}
