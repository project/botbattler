
(function() {

  'use strict';

  Drupal.behaviors.botbattler = {
    attach: function (context, settings) {
      document.addEventListener('readystatechange', _ => {
        if (document.readyState == 'complete')  {
          const forms = settings.botbattler.forms
          for (let id in forms) {
            botbattler(forms[id].action, forms[id].submitWait, id)
          }
        }
      })
    }
  }

})()