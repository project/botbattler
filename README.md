  *BotBattler is an effective, invisible, lightweight, 100% clientside, anti-spam solution for forms.*  
  
  The BotBattler module uses the git project BotBattler-JS at its core.
  Visit <a href="https://github.com/RikdeBoer/BotBattler-JS">BotBattler-JS</a>
  to learn more about how BotBattler works.

  **Installation and configuration**    
  When you enable the module BotBattler spam protection will be activated for
  ANONYMOUS users only, for all public forms.
  If you wish to activate protection for additional roles, visit 
  *People->Permissions*. Note that the Administrator role is always
  exempt from spam protection.
  
  BotBattler measures how many seconds pass between the time the page was loaded
  and the time the form was submitted. If the form was populated in, say, less 
  than 4 seconds (the default), it was most likely a bot at work and the
  submission will be blocked. You can set the "submit wait time" on the 
  *Configuration->BotBattler Settings* page.
  A higher value increases the probability of trapping bots. But setting it too
  high will inconvenience your users, as they'll be getting a "Form not ready"
  alert.

  **Console error**  
  In the Chrome and Safari browsers you may see a message like this in the 
  console: 
  *"An invalid form control with name='botbattler-name' is not focusable."*  
  You can safely ignore this message.
   
  **Can I use BotBattler in combination with other spam blockers?**  
  You should be able to. Be aware that most spam blockers operate serverside,
  whereas BotBattler is a clientside solution. This means that when 
  BotBattler traps a bot, the server and therefore any serverside spam blockers
  will never know about it and will have no work to do.
